//--------------------------------------------------------------------------
//-- geometries.js
//-- last updated Tue Feb 16 03:09:34 PM JST 2021
//--------------------------------------------------------------------------


//--------------------------------------------------------------------------
// algorithmic data tools
// often to be used for generation of rhythms

// import Timo Hoogland's Total Serialism library

const t = TotalSerialism;

// for lerp of floats use t.t.Generative.spreadInclusiveFloat

// let pick = inputArray => inputArray[Math.round((inputArray.length - 1) * Math.random())];

function pick (inputArray) {return inputArray[Math.round((inputArray.length - 1) * Math.random())];}

let pickN = (n,inputArray) => {
        let a = new Array(n);
        a.fill(0,0,n);
        let out = [];
        a.forEach(i => out.push(pick(inputArray)));
        return out }

function numArray (start,end) {
    let output = [];
    for (let i = start; i <= end; i++) {output.push(i)};
    return output
}

function low2HighSort (inputArray) { return inputArray.sort((a, b) => a - b)}

function high2LowSort (inputArray) { return inputArray.sort((a, b) => b - a)}

function takeN (inputArray, n) {
    let outputArray = [];
    for (let i = 0; i < n; i++)
    { outputArray.push(inputArray[i%(inputArray.length)])};
    return outputArray
}

function zip (a,b) {return a.map((x, i) => { return [x, b[i]]; })}

function buildZip (a,b) {return a.map((x,i) => x.concat(b[i]))}

class TimespanMap {
    constructor(span,prop,vals) {
        this.timespan = span;
        this.proportionalTimes = prop;
        this.calculatedTimes = prop.map(x => x * span);
        this.values = vals;
        }
    lookup(time) {
        let lookupTime = time%this.timespan;
        let filteredTime = this.calculatedTimes.filter(x => x < lookupTime);
        if (filteredTime[0] == undefined) {filteredTime = [0]};
        return this.values[(filteredTime.length - 1)]
        }
}

function timespanMapLookup (tsm, time) {
    let lookupTime = time%tsm.timespan;
    let filteredTime = tsm.calculatedTimes.filter(x => x < lookupTime);
    if (filteredTime[0] == undefined) {filteredTime = [0]};
    return tsm.values[(filteredTime.length - 1)]
}

//--------------------------------------------------------------------------
// tone.js convenience functions

function beatToTransport (bs) { return "0:"+ bs + ":0" }

function transportToEvent (x) { return {time: x}}

function beatsToEvents (bs) {return bs.map(x => transportToEvent(beatToTransport(x)))}

function replaceEventsWith (part, inputArray) {
    part.clear(); 
    inputArray.map(beatToTransport).map(x => transportToEvent(x)).forEach(x => part.add(x))
    return
}

function replaceEventsWith2 (part, inputArray) {
    part.clear(); 
    inputArray.forEach(x => part.add(x))
    return
}

function transportToBeat (t) {
    let ts = t.split(':').map(parseFloat);
    let beatsFromMeasures = ts[0] * 4;
    let beats = ts[1];
    let beatsFrom16s = ts[2]/4;
    return beatsFromMeasures + beats + beatsFrom16s 
}

function inNMeasures (n) {
    let currentPosition = Tone.Transport.position;
    let ts = currentPosition.split(':');
    let currentMeasure = parseFloat(ts[0]);
    return (currentMeasure + n) + ":0:0"
}

// warning: this is often too soon for Tone to process without an error 
// calculation takes too long, 2 or more measures may be necessary
// if so, use functions above

function nextMeasure () {return inNMeasures(1)}

function timesPlusArgs (times,args) {
    let events = beatsToEvents (times);
    let argsB = takeN (args,events.length);
    let argsC = argsB.map(x => {return {args: x}});   
    return events.map((x,i) => Object.assign(x,argsC[i]))
}

function randomLoopPoints (p, minRange, maxRange) {
   let range = (maxRange - minRange) * Math.random();
   let bufDur = p._buffer.duration;
   let startPoint = bufDur * Math.random();
   let endPoint = undefined;
   if (startPoint + range > bufDur) {startPoint = bufDur - minRange; endPoint = bufDur} else {endPoint = startPoint + range};
   p.stop(); p.loopStart = startPoint; p.loopEnd = endPoint; p.start();
   return {start: startPoint, end:endPoint}
} 

function retriggerLoop (p, loopPointsObject) {
    p.stop(); 
    p.loopStart = loopPointsObject.start; p.loopEnd = loopPointsObject.end; p.start();
}

//--------------------------------------------------------------------------
// geometries data structures and basic functions

class Performance {
    constructor() {
        this.dancers = {};
        this.rhythms = {};
        this.vocabulary = {};
        }
    dancerNames() {return Object.keys(this.dancers)}
    rhythmNames() {return Object.keys(this.rhythms)}
    vocabularyList() {return Object.keys(this.vocabulary)}
    newdancer(dancer) {
        console.log(`adding a new dancer called ${dancer}... `);
        this.dancers[dancer] = new Dancer()
        }

    startAll (transportTime) {
        let dNames = Object.keys(this.dancers);
        dNames.forEach(x => this.dancers[x].part.start(transportTime)) 
    }

    stopAll (transportTime) {
        let dNames = Object.keys(this.dancers);
        dNames.forEach((x) => {
            let dancer = this.dancers[x];
            dancer.part.stop(transportTime)
        }) 
    }
}

class Dancer {
    constructor() {
        this.part = undefined;
        this.rhythm = undefined;
        this.vocabulary = undefined;
        this.output = undefined;
        this.args = [];
        }

    start (transportTime) {
            this.part.start(transportTime)
        } 

    stop (transportTime) {
            this.part.stop(transportTime)
        } 

    partToggle () {
        if (this.part.state == "stopped") 
        {this.part.start(inNMeasures(2))}
        else {this.part.stop(inNMeasures(2))}
    }

    changeRhythmAndArgs (rhythm, args) {
        let events = timesPlusArgs(rhythm,args);
        let targetPart = this.part;
        replaceEventsWith2(targetPart,events)
    }

    changeVocab (vocab) {
        this.part.callback = vocab
        }
}

function fullVocab (vocab) {
    return ((time,value) => {Object.values(vocab).forEach(x => x(time,value))})
}

//--------------------------------------------------------------------------
