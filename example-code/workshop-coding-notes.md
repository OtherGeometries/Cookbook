# Workshop Coding Notes


## a brief introduction to Other Geometries components: 
	
* __stage__ { specific HTML sections are set as stage by defining, for eg.: `<div class="stage01">` };
	
*  __props__ { aka (theatrical) property, it is an object used on `stage` or `screen`; for eg.: text material; image; iframes; sounds... };

* __choreographic glossary__ { list of `movement functions` or `actions` which use _props_ that are placed in different _stages_; actions can generate for eg.: audio and visuals); and be called for eg.: breathing ( ) };

* __dancers__ { dancers are algorithmic character that compose sequence of `movement functions` or `actions`  };
		
* __performance score__  { is the sequence by which `movement functions` or `actions` are performed by dancers; altogether forming a complex choreographic relational filed };


## notes on making of a DANCER

**Web Console**

How to access the webconsole? 

* Firefox: on the top menu of the browser select "Tools" > "Web Developed" > "Web Console";
* Chrome: on the top menu of the browser select "View" > "Developer" > "Developer Tools";

[Check tutorial here](https://developer.mozilla.org/en-US/docs/Tools/Web_Console);

### Live Coding on the Web Console 

// selecting an HTML element;

// you can select the element by its 'class'; 'ID', or tagname [+++](https://www.w3schools.com/jsref/met_document_queryselector.asp);

    document.querySelector("#text");

    document.querySelector("#image");


// setting a variable;

// you can choose a different name for your variable [+++](https://www.w3schools.com/js/js_variables.asp);

    let x = document.querySelector("#text");

    let y = document.querySelector("#image");


// now lets change the style — there are a number of possibilities [+++](https://www.w3schools.com/css/default.asp);

// we will focus on the transform property [+++](https://www.w3schools.com/cssref/css3_pr_transform.asp);

// for the text we will change the scale [+++](https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/scale()); value for scale: "1" means normal size, "< 1" for eg.: 0.8 ; "> 1" for eg.: 3;

    x.style.transform="scale(3)"; 

// using property transform "skew"  [+++](https://developer.mozilla.org/en-US/docs/Web/CSS/angle);  values for skewing should be between "0deg" and "360 deg", and can also be negative values;

    y.style.transform="skew(35deg)";


// TEXT making into a function:

    function breathing (size) {
        document.querySelector("#text").style.transform="scale(" + size + ")";
    }

// example of setting a value to the function abover:

    breathing ("0.3")

// IMAGE making into a function:

    function decentering (deg) {
        document.querySelector("#image").style.transform="skew(" + deg + "deg)";
    }

// example of setting a value to the function abover:

    decentering ("190")

// now we will add this function that manipulates the DOM to the vocabulary;

### Creating a Vocabulary

// now we lets open "geometriesStarter.html"

// and add a new section for the code:

    <script type="text/javascript">
    </script>



// initiate storage of vocabulary

// and populate it with initial data

// create the empty vocabulary object first
    
    let vocabulary = {};

// add the various vocabulary words to the vocabulary;

// first create an object for the word;

// then add instances for each word to the corresponding object;

// note that there are instances for DOM (changing the browser window contents) and tone (making sound);

// each instance should be a function;

// at the moment, it should have two arguments: one for time and one to consume items from a parameter array;

// we will name this action applied to the text as BREATHING

    let x = document.querySelector("#text");

    let y = document.querySelector("#image");

    let z = document.querySelector("body");

    vocabulary["breathing"] = {}
    vocabulary["breathing"].DOM =
        ((time,value) => { Tone.Draw.schedule(() => {
		    x.style.transform=`scale(${value.args[0]})`;
   		    console.log('dancer 101: breathing ()');
         }, time);
     })

// we will name this action applied to the image as  DECENTERING

    vocabulary["decentering"] = {}
    vocabulary["decentering"].DOM =
        ((time,value) => { Tone.Draw.schedule(() => {
         y.style.transform = `skew(${value.args[0]})`; 
             console.log('dancer 102: decentering ()');
        }, time);
     })


// initiate storage of musical data

// and populate it with initial data

    let m = {
          args: {}
        , motifs: {}
        , players: {}
        , rhythms: {}
        , sequences: {}
        , synths: {}
        , samples: {}
        , timespans: {}
    }

// and add some rhythms

// generate some random rhythms for some dancers to follow

    m.rhythms.rhythm1 = [0,1,2,3,4,5,6,7];
    m.rhythms.rhythm2 = [0];
    m.rhythms.rhythm3 = [0].concat(low2HighSort(numArray(0,16).map(x => Math.random()).map(x => x * 16)));


### Generate this performance

    let p = new Performance();

// create the dancers to be used;

    p.newdancer('dancer101')

### Configure dancers 

// dancer101;

    p.dancers.dancer101.rhythm = m.rhythms.rhythm1.slice() 
    p.dancers.dancer101.part = new Tone.Part(
        vocabulary["breathing"].DOM
        , beatsToEvents(p.dancers.dancer101.rhythm)
    )
    p.dancers.dancer101.part.loop = true;
    p.dancers.dancer101.part.loopEnd = "2m";
    p.dancers.dancer101.part.probability = 1;
    p.dancers.dancer101.changeRhythmAndArgs([0,1,2,3,4,5,6,7], [["0.5"],["0.9"],["0.05"],["1"],["5"]])



### Start the dancer;

// two things necessary to get tone to start;

// to be done in the web console;

    Tone.start()
    Tone.Transport.start()

// actually start the dancer

    p.dancers.dancer101.start(inNMeasures(2))

// and to stop all dancers:

    p.stopAll()

// to start all dancers again:

    p.startAll(inNMeasures(2))

// for debugging type on the web console: 

    p.dancers.dancer101.part.state

// - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - -  - - - 

// connect the buttons to functions that control the performance

    document.getElementById("b1").addEventListener("click", () => {console.log("Tone started");Tone.start()});
    document.getElementById("b2").addEventListener("click", () => {console.log("starting dancers and player");Tone.Transport.start()
        p.dancers.dancer101.part.start(inNMeasures(3))
        // p.dancers.dancer102.part.start(inNMeasures(3))
    });
    document.getElementById("b3").addEventListener("click",() =>  {console.log("stopping dancers and player");
        Object.keys(m.players).forEach(x => m.players[x].stop()); Tone.Transport.stop()
    });



// Now we focus on the audio — > the players, play segments of specified mp3 files in loops (uses Tone.js)

// we will add this on top of the script (follow order of code on index.html):

    m.players.player1 = new Tone.Player({
        url: "./novaAmbient-170718b-chimePad.mp3",
        onload: console.log("m.synths.player1 is loaded."),
        loop: true,
        loopStart: 0.0,
        loopEnd: 0.7,
        fadeIn: 0.1,
        fadeOut: 0.1
        }).toDestination();

// to test is player is working, type in the web console:

    m.players.player1.start(inNMeasures(2))


	vocabulary["circumscribing_phase01"] = {}
	vocabulary["circumscribing_phase01"].tone = 
    	((time, value) => { 
       		randomLoopPoints (m.players.player1, 0.6, 2.5); 
        	// randomLoopPoints (m.players.player1b, 0.8, 3.5); 
        	console.log("dancer105, circumscribing_phase01; changed the loop at:" + Tone.Transport.position) 
    });


// we will add a new dancer for the sound files:

	p.newdancer('dancer105')


	p.dancers.dancer105.rhythm = m.rhythms.rhythm2.slice() 
	p.dancers.dancer105.part = new Tone.Part(
	    vocabulary["circumscribing_phase01"].tone
	    // fullVocab(vocabulary["circumscribing_phase01"])
	    , beatsToEvents(p.dancers.dancer105.rhythm)
	)
	p.dancers.dancer105.part.loop = true;
	p.dancers.dancer105.part.loopEnd = "1m";

// you can type on the web console or add this line to the "b2" button (Tone.Transport.start()):

	p.dancers.dancer105.part.start(inNMeasures(3))

// now we add a melody:

	m.motifs.riffA2 = ["A1","B1","E1","E#1","G1","A2","B2","E2","E#2","G3","A3"];

// we add effects:

	let verb = new Tone.JCReverb(0.6).toDestination();
	let comp = new Tone.Compressor(-20, 3).toDestination();

// and a sampler:

	m.synths.synth04 = new Tone.Sampler({
		urls: {
			E2: "circuloSubKick03-E2.mp3",
		},
		baseUrl: "./circuloSubKick03/"
	}).connect(comp);


// adding new vocabulary to be used by new dancer 103

	vocabulary["breathing_phase01"] = {};
	vocabulary["breathing_phase01"].tone =
    	((time, value) => { m.synths.synth04.triggerAttackRelease(pick(m.motifs.riffA2), "8n", time, value.velocity); 
        console.log("dancer103, breathing: " + Tone.Transport.position);
    });


// making dancer103

	p.newdancer('dancer103')

	p.dancers.dancer103.rhythm = m.rhythms.rhythm1.slice() 
	p.dancers.dancer103.part = new Tone.Part(
   		vocabulary["breathing_phase01"].tone
    	, beatsToEvents(p.dancers.dancer103.rhythm)
	)
	p.dancers.dancer103.part.loop = true;
	p.dancers.dancer103.part.loopEnd = "2m";


// add dancer to start button on type on the console:

	p.dancers.dancer103.part.start(inNMeasures(3))


// now we can create a sequence
// "sequencing" vocabulary allows you to set different phases to the piece which a dancer can step through


    vocabulary["sequencing"] = {}
    vocabulary["sequencing"].DOM =
          ((time,value) => { Tone.Draw.schedule(() => { 
               console.log("dancer109 (sequencer dancer using vocab 'squence') ran at " + Tone.Transport.position);

    // controls how many phases
    let numberOfPhases = 2;
    let trigger = (sequence1.triggered)%numberOfPhases;
    sequence1.triggered = trigger + 1
    if (trigger == 0) {

// here is PHASE 1

    p.dancers.dancer103.part.callback = vocabulary["breathing_phase01"].tone;


    m.players.player1.start(inNMeasures(3));
    p.dancers.dancer101.part.start(inNMeasures(3));
    p.dancers.dancer105.part.start(inNMeasures(3));
    p.dancers.dancer102.part.stop(inNMeasures(3));
    p.dancers.dancer103.part.loopEnd = "1m";
    p.dancers.dancer103.part.probability = 0.95
    p.dancers.dancer105.part.probability = 0.15

    console.log("--- PHASE 1 ---")
    }
    else if (trigger == 1) {

// here is PHASE 2

    p.dancers.dancer103.part.callback = vocabulary["breathing_phase02"].tone;

    m.players.player1.stop(inNMeasures(3));
    p.dancers.dancer102.part.start(inNMeasures(3));
    p.dancers.dancer101.part.stop(inNMeasures(3));
    p.dancers.dancer105.part.stop(inNMeasures(3));
    p.dancers.dancer103.part.loopEnd = "2m";
    p.dancers.dancer103.part.probability = 0.9
    p.dancers.dancer106.part.probability = 0.15

    console.log("--- PHASE 2 ---")
    }

    // you can add this last bit too — useful for debugging

    else {
    console.log("this should never run")
    }
        }, time);
    })


// now we create a 'macro' dancer that is responsible for sequencing:

    p.newdancer('dancer109')

    p.dancers.dancer109.rhythm = m.rhythms.rhythm2.slice() 
    p.dancers.dancer109.part = new Tone.Part(
        vocabulary["sequencing"].DOM
        , beatsToEvents(p.dancers.dancer109.rhythm)
    )
    p.dancers.dancer109.part.probability = 1;
    p.dancers.dancer109.part.loop = true;
    p.dancers.dancer109.part.loopEnd = "16m";

// at last add a global variable (next to the variables on top of the script):

    let sequence1 = {triggered: 0};

// at this point you can replace all other dancers in the button to dancer 109 (as this is responsible for starting and stopping all dancers)

	p.dancers.dancer109.part.start(inNMeasures(3))


## Further Links

[Useful Link on Web Programming](https://www.w3schools.com/)

[Tone.js: a library for making music in the browser, wrapping web audio](https://tonejs.github.io/)


