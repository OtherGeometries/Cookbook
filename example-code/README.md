# Other Geometries - Example Code

Choreographies of the Circle & Other Geometries — Example code by Joana Chicau and Renick Bell for online workshop (2021). Participants are challenged to develop a collaborative audiovisual code in a peer-to-peer environment, programmed in JavaScript.

More info on the research project [in this link](https://geometries.xyz) and on future workshops can be found [in here](https://geometries.xyz/workshops.html).


*::keyterms::*  live coding, audio-visual performance, collaborative real-time composition, dual interface, hybrid languages, dual syntax, web-environments, graphic design, choreography, computer programming, scores & scripts, performance and performativity, behaviours, collaboration, inclusion, exclusion, practices, principles, procedures, processes, respect, social responsibility, sustainability, trust. 
