# Critical Coding Cookbook

_Intersectional Feminist Approaches to Teaching and Learning_

## Title: Choreographies of the Circle & Other Geometries [*] 

_Project initiated by Joana Chicau & Renick Bell_

[*] link to project: https://www.geometries.xyz/ 

__Short description__

Choreographies of the Circle & Other Geometries is a research project initiated by Joana Chicau & Renick Bell on socio-technical protocols for collaborative audio-visual live coding and a corresponding peer-to-peer environment programmed in JavaScript.

The project reflects how language boundaries are enacted through the computing environment and within social spheres. It explores how movement, gestures, discourses, and behaviours are choreographed and communicated through the apparatuses at work and how our hybrid digital systems and transdisciplinary research practices co-construct each other. It is informed by on-going research and recollection of musical and choreographic sources and scores that reference principles of non-linear composition, non-hegemonic time and space constructs, techno-feminist understandings.


__Keywords:__ #audioVisual #algorithmicPerformance #collaborativeCompositionTools #esolang
